﻿using System.Text.Json;

namespace linq_examples
{
	public class UsersRepository
	{
		public List<User> GetAll()
		{
			var text = File.ReadAllText("users.json");

			return JsonSerializer.Deserialize<List<User>>(text);
		}
	}
}
