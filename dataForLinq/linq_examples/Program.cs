﻿namespace linq_examples
{
	internal class Program
	{
		static void Main(string[] args)
		{
			var usersRepository = new UsersRepository();

			var users = usersRepository.GetAll();

			// lets the linq journey begins
			// ...
		}
	}
}
